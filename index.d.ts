declare module 'url:*' {
    export default string;
}

declare module '*.png' {
    const value: any;
    export = value;
}
