import './number.extensions';
import { WIDTH, HEIGHT } from './main';
import { ColorMatrixFilter, Graphics, Point } from 'pixi.js';
import { Actor } from './actors';
import { animate, easeOut, linear } from 'popmotion';

export const collision_actions: {
    [key: string]: { [key: string]: (data: Actor[], opt?: any) => void };
} = {
    head: {
        head: () => {}
    },
    fist: {
        head: collisionFistHead,
        leg: () => {},
        fist: () => {},
        torso: () => {}
    },
    leg: {
        head: () => {},
        fist: () => {},
        leg: () => {},
        torso: () => {}
    }
};

function stopAll(data: Actor) {
    for (const key in data.actions) {
        const action = data.actions[key];
        action ? action.stop() : null;
    }
}

export function checkEdges(
    data: Actor,
    offx: number = 0,
    offy: number = 0
): boolean {
    const p = data.position;
    return (
        p.x > offx && p.y > offy && p.x < WIDTH - offx && p.y < HEIGHT - offy
    );
}

export function handleEdgeOverlap(data: Actor, w: number = 0, h: number = 0) {
    if (!data.actions.jump) {
        data.position.y < h
            ? (data.position.y = h + 1)
            : data.position.y > HEIGHT - h
            ? (data.position.y = HEIGHT - h - 1)
            : null;
        data.speed = 0;
    }
    data.position.x < w
        ? (data.position.x = w + 1)
        : data.position.x > WIDTH - w
        ? (data.position.x = WIDTH - w - 1)
        : null;
}

export function drawHitAreas(data: Actor, graphics: Graphics) {
    if (data.active_hit_areas!.length > 0) {
        const dir = data.animations![data.active_sprite].scale.x;
        for (const key of data.active_hit_areas!) {
            const area = data.hit_areas![key];
            const pos = area.positions[data.active_sprite][0];
            graphics.beginFill(0xffffff, 1);
            graphics.drawRect(
                data.root.position.x + (dir > 0 ? pos.x : -area.area.x - pos.x),
                data.root.position.y + pos.y,
                area.area.x,
                area.area.y
            );
            graphics.endFill();
        }
    }
}

export function updatePosition(data: Actor) {
    const length =
        1 +
        Math.sqrt(
            Math.abs(data.direction.x) ** 2 + Math.abs(data.direction.y) ** 2
        ).mod(1);
    data.position.x += data.speed * (Math.sign(data.direction.x) / length);
    data.position.y += data.speed * (Math.sign(data.direction.y) / length);
    data.root.position.x = Math.round(data.position.x);
    data.root.position.y = Math.round(data.position.y);
    data.speed = data.speed.clamp(0, data.speed_max);
}

function hit(data: Actor) {
    //stopAll(data);
    data.direction.x *= -1;
    data.direction.y *= -1;
    data.speed = data.speed_max;
    const color = data.root.filters
        ? (data.root.filters[0] as ColorMatrixFilter)
        : null;
    color ? (color.enabled = true) : null;
    return animate({
        from: 1,
        to: 0,
        ease: easeOut,
        duration: 500,
        onUpdate: value => {
            color?.contrast(value * 5.5, false);
            data.speed *= value;
        },
        onComplete: () => {
            color ? (color.enabled = false) : null;
            data.actions.lock = null;
            data.direction.set(0, 0);
        },
        onStop: () => {
            color ? (color.enabled = false) : null;
            data.actions.lock = null;
            data.direction.set(0, 0);
        }
    });
}

function collisionFistHead(data: Actor[], area: Point) {
    const p1 = data[0].position;
    p1.x += area.x >> 1;
    p1.y += area.y >> 1;
    const p2 = data[1].position;
    p2.x -= area.x >> 1;
    p2.y -= area.y >> 1;
    data[0].actions.lock = hit(data[0]);
    data[1].actions.lock = hit(data[1]);
    //move(data);
}

export function punch(data: Actor) {
    data.active_hit_areas!.push('fist');
    data.actions.punch = animate({
        from: 0,
        to: 1,
        duration: 250,
        onUpdate: v => {},
        onComplete: () => {
            data.actions.punch = null;
            data.active_hit_areas!.splice(
                data.active_hit_areas!.indexOf('fist'),
                1
            );
        },
        onStop: () => {
            data.actions.punch = null;
            data.active_hit_areas!.splice(
                data.active_hit_areas!.indexOf('fist'),
                1
            );
        }
    });
}

export function jump(data: Actor) {
    //data.actions.move ? data.actions.move.stop() : null;
    const y = data.position.y;
    const f = data.friction;
    data.friction = 0.99;
    //data.direction.y = 0;
    const v =
        data.controller?.axesStatus[0] !== 0
            ? data.speed.clamp(0, data.speed_max)
            : data.speed_max;
    data.actions.jump = animate({
        from: 0,
        to: -1,
        repeat: 1,
        repeatType: 'reverse',
        duration: Math.pow(v, 0.5) * 200,
        ease: easeOut,
        onUpdate: value => {
            data.speed *= 1.01;
            data.position.y = y + value * Math.abs(v) * 30;
        },
        onComplete: () => {
            data.actions.jump = null;
            data.friction = f;
            data.controller?.axesStatus[0] == 0 ? (data.speed *= 0.5) : null;
        },
        onStop: () => {
            data.actions['jump'] = null;
            data.friction = f;
        }
    });
}

export function move(data: Actor) {
    const a = data.controller.axesStatus;
    a && a[0] ? (data.direction.x = Math.round(a[0])) : null;
    a && a[1] ? (data.direction.y = Math.round(a[1])) : null;
    const x = a ? Math.abs(data.direction.x) : 0;
    const y = a ? Math.abs(data.direction.y) : 0;
    const d = x < y ? y : x;
    data.direction.x !== 0 && data.animations
        ? (data.animations.walk.scale.x = data.direction.x)
        : null;
    data.actions['move'] = animate({
        from: 0,
        to: 1,
        duration: 100 + (data.speed < data.speed_max / 4 ? 0 : d * 150),
        ease: linear,
        onUpdate: value => {
            a && (data.direction.x !== 0 || data.direction.y !== 0)
                ? (data.speed += d * data.acceleration)
                : null;
            if (data.animations) {
                const animation = data.animations.walk;
                const frame = Math.floor(value * (animation.totalFrames - 1));
                !data.actions.jump && frame.mod(3) == 0
                    ? animation.gotoAndStop(
                          (animation.currentFrame + 1).mod(
                              animation.totalFrames
                          )
                      )
                    : null;
            }
        },
        onComplete: () => {
            data.actions['move'] = null;
            //data.actions.jump ? null : data.direction.set(0,0);
        },
        onStop: () => {
            data.actions['move'] = null;
        }
    });
}
