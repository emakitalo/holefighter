import { Application } from 'pixi.js';
import start from './game';

const scale = 0.25;

export const WIDTH = 1080 * scale;
export const HEIGHT = 1920 * scale;

window.onload = async () => {
    const app = new Application({ width: WIDTH, height: HEIGHT });
    await start(app);

    const root = document.querySelector('main') as HTMLElement;
    root.appendChild(app.view as HTMLCanvasElement);
};
