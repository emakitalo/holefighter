import { Point } from 'pixi.js';
import { Actor, HitArea } from './actors';
import { collision_actions } from './actions';
import { V2 } from './utils';

function intersects(p1: Point, d1: Point, p2: Point, d2: Point): Point | null {
    // assume `a` and `b` are instances of Rectangle
    const w1 = p1.x + d1.x;
    const w2 = p2.x + d2.x;
    const rightmostLeft = p1.x < p2.x ? p2.x : p1.x;
    const leftmostRight = w1 > w2 ? w2 : w1;

    if (leftmostRight <= rightmostLeft) {
        return null;
    }

    const h1 = p1.y + d1.y;
    const h2 = p2.y + d2.y;
    const bottommostTop = p1.y < p2.y ? p2.y : p1.y;
    const topmostBottom = h1 > h2 ? h2 : h1;

    return topmostBottom > bottommostTop ? new Point(w1 - w2, h1 - h2) : null;
}

export async function checkCollisions(data: Actor[]) {
    for (let i = 0; i < data.length; i++) {
        const a1 = data[i];
        const areas: string[] | null = a1.active_hit_areas
            ? a1.active_hit_areas
            : null;
        if (areas) {
            for (const key1 of areas) {
                const area1 = a1.hit_areas![key1];
                for (const key2 of area1.collides) {
                    for (let j = 0; j < data.length; j++) {
                        if (i !== j) {
                            const a2 = data[j];
                            if (a2.active_hit_areas!.includes(key2)) {
								const area2 = a2.hit_areas![key2];
                                const area1_start = {
                                    ...area1.positions[a1.active_sprite][0]
                                } as Point;
                                const area2_start = {
                                    ...area2.positions[a2.active_sprite][0]
                                } as Point;
                                const s1 = a1.animations![a1.active_sprite]
                                    .scale;
                                const s2 = a2.animations![a2.active_sprite]
                                    .scale;
                                s1.x === -1
                                    ? (area1_start.x =
                                          -area1.area.x - area1_start.x)
                                    : null;
                                s2.x === -1
                                    ? (area2_start.x =
                                          -area2.area.x - area2_start.x)
                                    : null;
                                const overlap = intersects(
                                    V2.add(a1.root.position, area1_start),
                                    area1.area,
                                    V2.add(a2.root.position, area2_start),
                                    area2.area
                                );
                                if (overlap) {
                                    const action =
                                        collision_actions[key1][key2];
                                    action && !a1.actions.lock
                                        ? action([a1, a2], overlap)
                                        : null;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
