declare interface Number {
    mod(n: number): number;
    clamp(min: number, max: number): number;
}

Number.prototype.clamp = function(min: number, max: number) {
    return Math.min(Math.max(this as number, min), max);
};

Number.prototype.mod = function(n: number) {
    return (((this as number) % n) + n) % n;
};
