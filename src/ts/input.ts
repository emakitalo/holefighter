import { Actor } from "./actors";
export interface Controller {
    turbo: boolean;
    buttonsCache: number[];
    buttonsStatus: number[];
    axesStatus: number[];
    axesCache: number[];
}

export interface Pad extends Controller {
    controller: Gamepad;
}

const limit = 0.1;

export function connectPads(data: Actor[], pads: Pad[]) {
    window.addEventListener('gamepadconnected', (e: GamepadEvent) => {
        console.log('Pad : ', e.gamepad, ' connected');
        const pad: Pad = {
            controller: e.gamepad,
            turbo: true,
            buttonsCache: [],
            buttonsStatus: [],
            axesStatus: [],
            axesCache: []
        };
        pads.push(pad);
		data[pads.length - 1].controller = pad;
    });

    window.addEventListener('gamepaddisconnected', (e: GamepadEvent) => {
        let i: number = -1;
        for (let id = 0; id < pads.length; id++) {
            console.log(pads[id].controller === e.gamepad);
            e.gamepad == pads[id].controller ? (i = id) : null;
        }
        pads.splice(i, 1);
    });
}

export function axesActive(axes: number[] | undefined): boolean {
    if (axes)
        return (
            axes[0] > limit ||
            axes[0] < -limit ||
            axes[1] > limit ||
            axes[1] < -limit
        );
    else return false;
}

export function updateControllers(pads: Pad[]) {
    const currentPads = navigator.getGamepads();
    for (const pad of pads) {
        pad.controller = currentPads[pad.controller.index] as Gamepad;
        pad.buttonsCache = [...pad.buttonsStatus];
        pad.axesCache = [...pad.axesStatus];
        pad.buttonsStatus = [];
        const c = pad.controller;

        const pressed = [];
        if (c.buttons) {
            for (const b of c.buttons) {
                b.pressed ? pressed.push(c.buttons.indexOf(b)) : null;
            }
        }
        pad.buttonsStatus = pressed;

        const axes: number[] = [];
        if (c.axes) {
            for (const a of c.axes) {
                axes.push(a);
            }
        }
        pad.axesStatus = axes;
    }
}
