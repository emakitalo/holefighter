import { Point } from 'pixi.js';
import { createActor, addAnimations, Actor } from '../actors';
import sprite from '../../sprites/zombie.png';
import json from '../../sprites/zombie.json';

export default async (): Promise<Actor> => {
    const zombie = createActor({
        animations: await addAnimations('zombie', sprite, json),
		active_hit_areas: ['head'],
        hit_areas: {
            head: {
                area: new Point(10,10),
                positions: {
                    walk: [new Point(-5,-25)]
                },
                collides: [],
            },
            fist: {
                area: new Point(10,10),
                positions: {
                    walk: [new Point(15,0)]
                },
                collides: ['head'],
            }
        }
    });
    return zombie;
};
