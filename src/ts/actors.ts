import {
    Assets,
    Spritesheet,
    AnimatedSprite,
    ISpritesheetData,
    Container,
    Point
} from 'pixi.js';
import { Controller } from './input';

export interface HitArea {
    area: Point;
    positions: { [key: string]: Point[] };
    collides: string[];
}

export interface Actor {
    animations?: { [key: string]: AnimatedSprite };
    active_sprite: string;
    actions: { [key: string]: { stop: () => void } | null };
	position: Point;
    direction: Point;
    acceleration: number;
    speed: number;
    speed_max: number;
    jump_speed: number;
    friction: number;
    root: Container;
    controller: Controller;
	active_hit_areas?: string[];
    hit_areas?: { [key: string]: HitArea };
}

const sheets: { [key: string]: Spritesheet } = {};

export function createActor(data: { [keys: string]: any }): Actor {
    !data.active_sprite ? data.active_sprite = "walk" : null;
    !data.actions ? (data.actions = {}) : null;
    !data.position ? (data.position = new Point()) : null;
    !data.direction ? (data.direction = new Point()) : null;
    !data.speed ? (data.speed = 1) : null;
    !data.speed_max ? (data.speed_max = 2) : null;
    !data.jump_speed ? (data.jump_speed = 0) : null;
    !data.friction ? (data.friction = 0.9) : null;
    !data.acceleration ? (data.acceleration = 0.01) : null;
    !data.root ? (data.root = new Container()) : null;
    !data.controller
        ? (data.controller = {
              turbo: false,
              buttonsStatus: [],
              buttonsCache: [],
              axesStatus: [],
              axesCache: []
          })
        : null;
    return data as Actor;
}

export function cloneAnimations(name: string) {
    const sheet = sheets[name];
    const animations: { [key: string]: AnimatedSprite } = {};
    for (const name in sheet.animations) {
        const sprite = new AnimatedSprite(sheet.animations[name]);
        sprite.anchor.set(0.5, 0.5);
        animations[name] = sprite;
    }
    return animations;
}

export async function addAnimations(
    name: string,
    texpath: string,
    json: ISpritesheetData
) {
    const animations: { [key: string]: AnimatedSprite } = {};
    const tex = await Assets.load(texpath);
    const sheet = new Spritesheet(tex.baseTexture, json);
    sheet.parse();
    sheets[name] = sheet;
    for (const name in sheet.animations) {
        const sprite = new AnimatedSprite(sheet.animations[name]);
        sprite.anchor.set(0.5, 0.5);
        animations[name] = sprite;
    }
    return animations;
}
