import { Point } from "pixi.js";

function add(p1: Point, p2:Point): Point {
  return new Point(p1.x + p2.x, p1.y + p2.y);
}

function sub(p1: Point, p2:Point): Point {
  return new Point(p1.x - p2.x, p1.y - p2.y);
}

function mult(p1: Point, m:number): Point {
  return new Point(p1.x * m, p1.y * m);
}

export const V2 = {
  add: add,
  sub: sub,
  mult: mult
};
