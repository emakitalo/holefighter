import './number.extensions';
import { WIDTH, HEIGHT } from './main';
import {
    Application,
    DisplayObject,
    ColorMatrixFilter,
    Container,
    Graphics
} from 'pixi.js';
import Zombie from './actors/zombie';
import { updateControllers, axesActive, Pad, connectPads } from './input';
import { Actor, cloneAnimations, createActor } from './actors';
import {
    move,
    jump,
    punch,
    updatePosition,
    drawHitAreas,
    checkEdges,
    handleEdgeOverlap
} from './actions';
import { createSlider, add } from './gui';
import { checkCollisions } from './collision';
import { animate } from 'popmotion';
import { Menu, Table } from './menu';

const graphics = new Graphics();

const States = {
    MENU: 'MENU',
    GAME: 'GAME',
    SWITCH: 'SWITCH'
};

let ACTIVE_STATE: string | undefined = States.MENU;
let NEXT_STATE: string;
let ACTIVE_STAGE: Container;
let ACTIVE_BRANCH: Table;

function clone(name: string, data: { [key: string]: Actor }): Actor {
    const d = data[name];
    const aha = d.active_hit_areas ? [...d.active_hit_areas] : [];
    return createActor({
        animations: cloneAnimations(name),
        active_hit_areas: aha,
        hit_areas: JSON.parse(JSON.stringify(data[name].hit_areas))
    });
}

function updateMenu(pad: Pad) {
    const active_item = ACTIVE_BRANCH.nodes[ACTIVE_STAGE.getChildAt(0).name];
    if (pad.buttonsStatus.includes(0)) {
        active_item.emit('pointertap');
    }
    const y = pad.axesStatus[1];
    if (y && y !== 0) {
        active_item.interactive = false;
        ACTIVE_STAGE.setChildIndex(
            active_item,
            y < 0 ? ACTIVE_STAGE.children.length - 1 : 1
        );
        const next_item = ACTIVE_BRANCH.nodes[ACTIVE_STAGE.getChildAt(0).name];
        next_item.interactive = true;

        ACTIVE_STATE = undefined;

        animate({
            from: '#ff0000',
            to: '#ffffff',
            duration: 1000,
            onUpdate: v => {
                active_item.style.fill = v;
            }
        });

        animate({
            from: '#ffffff',
            to: '#ff0000',
            duration: 1000,
            onUpdate: v => {
                next_item.style.fill = v;
            },
            onComplete: () => {
                ACTIVE_STATE = States.MENU;
            }
        });
    }
}

async function updateGame(players: Actor[]) {
    graphics.clear();
    for (const player of players) {
        const buttons = player.controller?.buttonsStatus;
        buttons?.length ? console.log(buttons) : null;
        const moving = axesActive(player.controller?.axesStatus);
        const sprite = player.animations![player.active_sprite];
        const w = sprite.width / 2;
        const h = sprite.height / 2;
        if (checkEdges(player, w, h)) {
            if (!player.actions.lock) {
                moving && !player.actions.move && !player.actions.jump
                    ? move(player)
                    : null;
                !moving && !player.actions.move && !player.actions.jump
                    ? (player.speed *= player.friction)
                    : null;
                player.actions.jump ? (player.speed *= player.friction) : null;
            }
        } else {
            handleEdgeOverlap(player, w, h);
        }
        !player.actions.lock && buttons?.includes(0) && !player.actions.jump
            ? jump(player)
            : null;

        !player.actions.punch &&
        buttons?.includes(1) &&
        !player.controller.buttonsCache.includes(1)
            ? punch(player)
            : null;

        updatePosition(player);
        drawHitAreas(player, graphics);

        if (player.controller.buttonsStatus.includes(9)) {
            ACTIVE_STATE = States.SWITCH;
            NEXT_STATE = States.MENU;
        }
    }
    await checkCollisions(players);
}

export default async (pixi: Application) => {
    const menu = Menu.main.container;
    menu.name = States.MENU;
    menu.position.set(WIDTH / 2, HEIGHT / 2);
    Menu.main.nodes.SP.on('pointertap', () => {
        console.log('CLICKED');
        ACTIVE_STATE = States.SWITCH;
        NEXT_STATE = States.GAME;
    });
    const game = new Container();
    game.name = States.GAME;
    game.alpha = 0;
    game.visible = false;

    pixi.stage.addChild(menu);
    pixi.stage.addChild(game);
    pixi.stage.addChild(graphics);

    ACTIVE_STAGE = pixi.stage.getChildByName(ACTIVE_STATE!);
    ACTIVE_BRANCH = Menu.main;

    const pads: Pad[] = [];

    const zombie: Actor = await Zombie();
    const actors = {
        zombie: zombie
    };

    const players = [clone('zombie', actors), clone('zombie', actors)];
    let i = 0;
    for (const player of players) {
        const sprite = player.animations![player.active_sprite];
        player.position.x = sprite.width * i + (sprite.width >> 1);
        for (const key in player.animations) {
            const animation = player.animations[key];
            player.root.addChild(animation);
        }
        player.root.filters = [new ColorMatrixFilter()];
        game.addChild(player.root);
        i++;
    }

    connectPads(players, pads);

    const player1 = players[0];

    const STATES: { [key: string]: () => void } = {
        GAME: () => {
            updateGame(players);
            game.children.sort((a: DisplayObject, b: DisplayObject): number => {
                if (a.position.y > b.position.y) return 1;
                if (a.position.y < b.position.y) return -1;
                if (a.position.x > b.position.x) return 1;
                if (a.position.x < b.position.x) return -1;
                return 0;
            });
        },
        MENU: () => {
            updateMenu(player1.controller as Pad);
        },
        SWITCH: () => {
            ACTIVE_STATE = undefined;
            animate({
                from: 1,
                to: 0,
                duration: 1000,
                onUpdate: v => {
                    ACTIVE_STAGE.alpha = v;
                },
                onComplete: () => {
                    ACTIVE_STAGE.visible = false;
                    ACTIVE_STAGE = pixi.stage.getChildByName(NEXT_STATE);
                    ACTIVE_STAGE.visible = true;
                    animate({
                        from: 0,
                        to: 1,
                        duration: 1000,
                        onUpdate: v => {
                            ACTIVE_STAGE.alpha = v;
                        },
                        onComplete() {
                            ACTIVE_STATE = NEXT_STATE;
                        }
                    });
                }
            });
        }
    };

    pixi.ticker.add(() => {
        try {
            updateControllers(pads);
        } catch (e) {
            console.log(e);
        }
        ACTIVE_STATE ? STATES[ACTIVE_STATE]() : null;
    });

    setInterval(() => {
        players[1].controller.axesStatus = [
            (Math.random() - 0.5) * 2,
            (Math.random() - 0.5) * 2
        ];
    }, 5000);

    const player_settings = add({ type: 'ul', name: 'player_settings' });
    const gui = document.querySelector('.gui');
    gui?.appendChild(player_settings);

    const player_friction = createSlider(
        (value: number) => (player1.friction = value),
        { name: 'friction', min: 0, max: 1, step: 0.01 }
    );
    player_settings.appendChild(player_friction);

    const player_acceleration = createSlider(
        (value: number) => (player1.acceleration = value),
        { name: 'acceleration', min: 0, max: 0.2, step: 0.01 }
    );
    player_settings?.appendChild(player_acceleration);
};
