import { Container, Sprite, Text, TextStyle } from 'pixi.js';

const style: Partial<TextStyle> = {
    fontFamily: 'Arial',
    fontSize: 24,
    fill: 0xffffff,
    align: 'center'
};

export interface Table {
    container: Container;
    nodes: { [key: string]: Text };
}

const main = new Container();
const one = new Text('Single Player', style);
one.interactive = false;
one.name = 'SP';
one.anchor.set(0.5, 0.5);
const two = new Text('Two Players', style);
two.interactive = false;
two.name = 'TP';
two.anchor.set(0.5, 0.5);
two.position.y += one.position.y + one.height;
main.addChild(one);
main.addChild(two);

const main_nodes: { [key: string]: Text } = {};

main_nodes[one.name] = one;
main_nodes[two.name] = two;

export const Menu: { [key: string]: Table } = {
    main: {
        container: main,
        nodes: main_nodes
    }
};
