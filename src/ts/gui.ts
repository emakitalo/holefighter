interface Slider {
    name: string;
    min: number;
    max: number;
    step: number;
}

interface Element {
    type: string;
    name: string;
    data?: string;
}

export function add(data: Element) {
    const g = ['label', 'output'];
    const e = document.createElement(data.type);
    !(data.type in g) ? e.classList.add(data.name) : null;
    data.type in g ? e.setAttribute('for', data.name) : null;
    data.data ? (e.innerHTML = data.data) : null;
    return e;
}

export function createSlider(
    value: (value: number) => void,
    data: Slider
): HTMLElement {
    const c = add({ type: 'li', name: data.name });
    const l = add({ type: 'label', name: data.name });
    const i = add({ type: 'input', name: data.name }) as HTMLInputElement;
    const o = add({ type: 'output', name: data.name }) as HTMLOutputElement;

    l.innerHTML = data.name;
    i.setAttribute('type', 'range');
    i.setAttribute('name', data.name);
    i.setAttribute('min', data.min.toString());
    i.setAttribute('max', data.max.toString());
    i.setAttribute('step', data.step.toString());
	o.textContent = '0';
    i.addEventListener('input', () => {
        o.textContent = i.value;
        value(parseFloat(i.value));
    });

    c.appendChild(l);
    c.appendChild(i);
    c.appendChild(o);

    return c;
}
